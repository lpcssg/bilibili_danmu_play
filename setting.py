# bilibili番剧视频url
BILIBILI_URL = "https://www.bilibili.com/bangumi/play/ep791312?spm_id_from=333.337.0.0&from_spmid=666.25.episode.0"

# 视频和弹幕名字
FILE_NAME = ""

# 视频下载网址
VIDEO_URL = ""

# 弹幕xml转ass网址
DANMU_XML2ASS_URL = ""

# 视频和弹幕文件保存路径
SAVE_PATH = "./video/"

# potplayer播放器路径
POTPLAYER_PATH = ""

# 网络下载器配置
HERDER = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"
}
