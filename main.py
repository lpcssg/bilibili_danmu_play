from bilibili_dan_mu_down import get_danmu_by_cid,get_cid_by_bilibili_url
from dan_mu_xml2ass import dan_mu_xml2ass
import shutil
from setting import SAVE_PATH
import os
import subprocess

video_url='https://www.bilibili.com/bangumi/play/ep785546?spm_id_from=333.337.0.0&from_spmid=666.25.episode.0'

# 根据url获取cid
# 根据cid获取弹幕
cid=get_cid_by_bilibili_url(video_url)
file_name = get_danmu_by_cid(cid=cid)
print(file_name)
# 把弹幕转换成ass格式
dan_mu_xml2ass(
    dan_mu_xml_path=SAVE_PATH + file_name + "/", 
    dan_mu_xml_name=file_name + ".xml"
)

# 把重命名文件复制到相应目录
shutil.copyfile('./rename/rename.py',SAVE_PATH + file_name + "/rename.py")
shutil.copyfile('./rename/01_rename.bat',SAVE_PATH + file_name + "/01_rename.bat")
# 判断文件中是否有视频
src_dir='F:\\down\\edgeDown\\M3U8在线下载工具.mp4'
if os.path.exists(src_dir):
    shutil.move(src_dir,SAVE_PATH + file_name)
    subprocess.Popen("python rename.py",cwd=SAVE_PATH + file_name)
# 根据url获取m3u8链接
# 下载m3u8
# 使用potplayer播放