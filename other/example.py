__all__ = ['example']

# Don't look below, you will not understand this Python code :) I don't.

from js2py.pyjs import *
# setting scope
var = Scope( JS_BUILTINS )
set_global_object(var)

# Code follows:
var.registers(['generateASS', 'parseFile', 'calcWidth', 'fetchDanmaku', 'gotFile', 'funStr', 'fetchXML', 'parseXML', 'RRGGBB', 'config', 'startDownload', 'sideDanmaku', 'fillStr', 'normalDanmaku', 'choseFont', 'hypot', 'debug', 'hexAlpha', 'initFont', 'setPosition'])
var.put('config', Js({'playResX':Js(560.0),'playResY':Js(420.0),'fontlist':Js([Js('Microsoft YaHei UI'), Js('Microsoft YaHei'), Js('文泉驿正黑'), Js('STHeitiSC'), Js('黑体')]),'font_size':Js(1.0),'r2ltime':Js(8.0),'fixtime':Js(4.0),'opacity':Js(0.6),'space':Js(0.0),'max_delay':Js(6.0),'bottom':Js(50.0),'use_canvas':var.get(u"null"),'debug':Js(False)}))
@Js
def PyJs_anonymous_0_(this, arguments, var=var):
    var = Scope({'this':this, 'arguments':arguments}, var)
    var.registers([])
    pass
PyJs_anonymous_0_._set_name('anonymous')
var.put('debug', (var.get('console').get('log').callprop('bind', var.get('console')) if var.get('config').get('debug') else PyJs_anonymous_0_))
@Js
def PyJs_anonymous_1_(str, this, arguments, var=var):
    var = Scope({'str':str, 'this':this, 'arguments':arguments}, var)
    var.registers(['dict', 'str'])
    var.put('dict', var.get('Array').callprop('apply', var.get('Array'), var.get('arguments')))
    @Js
    def PyJs_anonymous_2_(r, o, this, arguments, var=var):
        var = Scope({'r':r, 'o':o, 'this':this, 'arguments':arguments}, var)
        var.registers(['ret', 'o', 'r'])
        pass
        @Js
        def PyJs_anonymous_3_(i, this, arguments, var=var):
            var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
            var.registers(['i'])
            return var.put('ret', var.get('i').get(var.get('o')))
        PyJs_anonymous_3_._set_name('anonymous')
        var.get('dict').callprop('some', PyJs_anonymous_3_)
        return (var.get('ret') or Js(''))
    PyJs_anonymous_2_._set_name('anonymous')
    return var.get('str').callprop('replace', JsRegExp('/{{([^}]+)}}/g'), PyJs_anonymous_2_)
PyJs_anonymous_1_._set_name('anonymous')
var.put('fillStr', PyJs_anonymous_1_)
@Js
def PyJs_anonymous_4_(color, this, arguments, var=var):
    var = Scope({'color':color, 'this':this, 'arguments':arguments}, var)
    var.registers(['t', 'color'])
    var.put('t', var.get('Number')(var.get('color')).callprop('toString', Js(16.0)).callprop('toUpperCase'))
    return (var.get('Array')(Js(7.0)).callprop('join', Js('0'))+var.get('t')).callprop('slice', (-Js(6.0)))
PyJs_anonymous_4_._set_name('anonymous')
var.put('RRGGBB', PyJs_anonymous_4_)
@Js
def PyJs_anonymous_5_(opacity, this, arguments, var=var):
    var = Scope({'opacity':opacity, 'this':this, 'arguments':arguments}, var)
    var.registers(['opacity', 'alpha'])
    var.put('alpha', var.get('Math').callprop('round', (Js(255)*(Js(1.0)-var.get('opacity')))).callprop('toString', Js(16.0)).callprop('toUpperCase'))
    return (var.get('Array')((Js(3.0)-var.get('alpha').get('length'))).callprop('join', Js('0'))+var.get('alpha'))
PyJs_anonymous_5_._set_name('anonymous')
var.put('hexAlpha', PyJs_anonymous_5_)
@Js
def PyJs_anonymous_6_(fun, this, arguments, var=var):
    var = Scope({'fun':fun, 'this':this, 'arguments':arguments}, var)
    var.registers(['fun'])
    return var.get('fun').callprop('toString').callprop('split', JsRegExp('/\\r\\n|\\n|\\r/')).callprop('slice', Js(1.0), (-Js(1.0))).callprop('join', Js('\n'))
PyJs_anonymous_6_._set_name('anonymous')
var.put('funStr', PyJs_anonymous_6_)
@Js
def PyJs_anonymous_7_(this, arguments, var=var):
    var = Scope({'this':this, 'arguments':arguments}, var)
    var.registers([])
    @Js
    def PyJs_anonymous_8_(x, y, this, arguments, var=var):
        var = Scope({'x':x, 'y':y, 'this':this, 'arguments':arguments}, var)
        var.registers(['y', 'x'])
        return (var.get('x')+(var.get('y')*var.get('y')))
    PyJs_anonymous_8_._set_name('anonymous')
    return var.get('Math').callprop('sqrt', Js([Js(0.0)]).callprop('concat', var.get('Array').callprop('apply', var.get('Array'), var.get('arguments'))).callprop('reduce', PyJs_anonymous_8_))
PyJs_anonymous_7_._set_name('anonymous')
var.put('hypot', (var.get('Math').get('hypot').callprop('bind', var.get('Math')) if var.get('Math').get('hypot') else PyJs_anonymous_7_))
@Js
def PyJs_anonymous_9_(data, filename, this, arguments, var=var):
    var = Scope({'data':data, 'filename':filename, 'this':this, 'arguments':arguments}, var)
    var.registers(['data', 'filename', 'url', 'saveas', 'blob'])
    var.put('blob', var.get('Blob').create(Js([var.get('data')]), Js({'type':Js('application/octet-stream')})))
    var.put('url', var.get('window').get('URL').callprop('createObjectURL', var.get('blob')))
    var.put('saveas', var.get('document').callprop('createElement', Js('a')))
    var.get('saveas').put('href', var.get('url'))
    var.get('saveas').get('style').put('display', Js('none'))
    var.get('document').get('body').callprop('appendChild', var.get('saveas'))
    var.get('saveas').put('download', var.get('filename'))
    var.get('saveas').callprop('click')
    @Js
    def PyJs_anonymous_10_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers([])
        var.get('saveas').get('parentNode').callprop('removeChild', var.get('saveas'))
    PyJs_anonymous_10_._set_name('anonymous')
    var.get('setTimeout')(PyJs_anonymous_10_, Js(1000.0))
    @Js
    def PyJs_anonymous_11_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers([])
        var.get('window').get('URL').callprop('revokeObjectURL', var.get('url'))
    PyJs_anonymous_11_._set_name('anonymous')
    var.get('document').callprop('addEventListener', Js('unload'), PyJs_anonymous_11_)
PyJs_anonymous_9_._set_name('anonymous')
var.put('startDownload', PyJs_anonymous_9_)
@Js
def PyJs_anonymous_12_(this, arguments, var=var):
    var = Scope({'this':this, 'arguments':arguments}, var)
    var.registers(['calcWidthDiv', 'calcWidthCanvas'])
    @Js
    def PyJs_anonymous_13_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers(['canvas', 'context'])
        var.put('canvas', var.get('document').callprop('createElement', Js('canvas')))
        var.put('context', var.get('canvas').callprop('getContext', Js('2d')))
        @Js
        def PyJs_anonymous_14_(fontname, text, fontsize, this, arguments, var=var):
            var = Scope({'fontname':fontname, 'text':text, 'fontsize':fontsize, 'this':this, 'arguments':arguments}, var)
            var.registers(['text', 'fontname', 'fontsize'])
            var.get('context').put('font', (((Js('bold ')+var.get('fontsize'))+Js('px '))+var.get('fontname')))
            return var.get('Math').callprop('ceil', (var.get('context').callprop('measureText', var.get('text')).get('width')+var.get('config').get('space')))
        PyJs_anonymous_14_._set_name('anonymous')
        return PyJs_anonymous_14_
    PyJs_anonymous_13_._set_name('anonymous')
    var.put('calcWidthCanvas', PyJs_anonymous_13_)
    @Js
    def PyJs_anonymous_15_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers(['d', 'ld'])
        var.put('d', var.get('document').callprop('createElement', Js('div')))
        var.get('d').callprop('setAttribute', Js('style'), Js([Js('all: unset'), Js('top: -10000px'), Js('left: -10000px'), Js('width: auto'), Js('height: auto'), Js('position: absolute'), Js('')]).callprop('join', Js(' !important; ')))
        @Js
        def PyJs_anonymous_16_(this, arguments, var=var):
            var = Scope({'this':this, 'arguments':arguments}, var)
            var.registers([])
            var.get('document').get('body').get('parentNode').callprop('appendChild', var.get('d'))
        PyJs_anonymous_16_._set_name('anonymous')
        var.put('ld', PyJs_anonymous_16_)
        if var.get('document').get('body').neg():
            var.get('document').callprop('addEventListener', Js('DOMContentLoaded'), var.get('ld'))
        else:
            var.get('ld')()
        @Js
        def PyJs_anonymous_17_(fontname, text, fontsize, this, arguments, var=var):
            var = Scope({'fontname':fontname, 'text':text, 'fontsize':fontsize, 'this':this, 'arguments':arguments}, var)
            var.registers(['text', 'fontname', 'fontsize'])
            var.get('d').put('textContent', var.get('text'))
            var.get('d').get('style').put('font', (((Js('bold ')+var.get('fontsize'))+Js('px '))+var.get('fontname')))
            return (var.get('d').get('clientWidth')+var.get('config').get('space'))
        PyJs_anonymous_17_._set_name('anonymous')
        return PyJs_anonymous_17_
    PyJs_anonymous_15_._set_name('anonymous')
    var.put('calcWidthDiv', PyJs_anonymous_15_)
    if PyJsStrictEq(var.get('config').get('use_canvas'),var.get(u"null")):
        if (var.get('navigator').get('platform').callprop('match', JsRegExp('/linux/i')) and var.get('navigator').get('userAgent').callprop('match', JsRegExp('/chrome/i')).neg()):
            var.get('config').put('use_canvas', Js(False))
    var.get('debug')(Js('use canvas: %o'), PyJsStrictNeq(var.get('config').get('use_canvas'),Js(False)))
    if PyJsStrictEq(var.get('config').get('use_canvas'),Js(False)):
        return var.get('calcWidthDiv')()
    return var.get('calcWidthCanvas')()
PyJs_anonymous_12_._set_name('anonymous')
var.put('calcWidth', PyJs_anonymous_12_())
@Js
def PyJs_anonymous_18_(fontlist, this, arguments, var=var):
    var = Scope({'fontlist':fontlist, 'this':this, 'arguments':arguments}, var)
    var.registers(['sampleFont', 'f', 'fontlist', 'diffFont', 'validFont', 'sampleText'])
    var.put('sampleText', (((((Js('The quick brown fox jumps over the lazy dog')+Js('7531902468'))+Js(',.!-'))+Js('，。：！'))+Js('天地玄黄'))+Js('則近道矣')))
    var.put('sampleFont', Js([Js('monospace'), Js('sans-serif'), Js('sans'), Js('Symbol'), Js('Arial'), Js('Comic Sans MS'), Js('Fixed'), Js('Terminal'), Js('Times'), Js('Times New Roman'), Js('宋体'), Js('黑体'), Js('文泉驿正黑'), Js('Microsoft YaHei')]))
    @Js
    def PyJs_anonymous_19_(base, test, this, arguments, var=var):
        var = Scope({'base':base, 'test':test, 'this':this, 'arguments':arguments}, var)
        var.registers(['base', 'test', 'testSize', 'baseSize'])
        var.put('baseSize', var.get('calcWidth')(var.get('base'), var.get('sampleText'), Js(72.0)))
        var.put('testSize', var.get('calcWidth')(((var.get('test')+Js(','))+var.get('base')), var.get('sampleText'), Js(72.0)))
        return PyJsStrictNeq(var.get('baseSize'),var.get('testSize'))
    PyJs_anonymous_19_._set_name('anonymous')
    var.put('diffFont', PyJs_anonymous_19_)
    @Js
    def PyJs_anonymous_20_(test, this, arguments, var=var):
        var = Scope({'test':test, 'this':this, 'arguments':arguments}, var)
        var.registers(['valid', 'test'])
        @Js
        def PyJs_anonymous_21_(base, this, arguments, var=var):
            var = Scope({'base':base, 'this':this, 'arguments':arguments}, var)
            var.registers(['base'])
            return var.get('diffFont')(var.get('base'), var.get('test'))
        PyJs_anonymous_21_._set_name('anonymous')
        var.put('valid', var.get('sampleFont').callprop('some', PyJs_anonymous_21_))
        var.get('debug')(Js('font %s: %o'), var.get('test'), var.get('valid'))
        return var.get('valid')
    PyJs_anonymous_20_._set_name('anonymous')
    var.put('validFont', PyJs_anonymous_20_)
    var.put('f', var.get('fontlist').get((var.get('fontlist').get('length')-Js(1.0))))
    var.put('fontlist', var.get('fontlist').callprop('filter', var.get('validFont')))
    var.get('debug')(Js('fontlist: %o'), var.get('fontlist'))
    return (var.get('fontlist').get('0') or var.get('f'))
PyJs_anonymous_18_._set_name('anonymous')
var.put('choseFont', PyJs_anonymous_18_)
@Js
def PyJs_anonymous_22_(this, arguments, var=var):
    var = Scope({'this':this, 'arguments':arguments}, var)
    var.registers(['done'])
    var.put('done', Js(False))
    @Js
    def PyJs_anonymous_23_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers([])
        if var.get('done'):
            return var.get('undefined')
        var.put('done', Js(True))
        var.put('calcWidth', var.get('calcWidth').callprop('bind', var.get('window'), var.get('config').put('font', var.get('choseFont')(var.get('config').get('fontlist')))))
    PyJs_anonymous_23_._set_name('anonymous')
    return PyJs_anonymous_23_
PyJs_anonymous_22_._set_name('anonymous')
var.put('initFont', PyJs_anonymous_22_())
@Js
def PyJs_anonymous_24_(danmaku, info, this, arguments, var=var):
    var = Scope({'danmaku':danmaku, 'info':info, 'this':this, 'arguments':arguments}, var)
    var.registers(['formatTime', 'escapeAssText', 'format', 'convert2Ass', 'info', 'paddingNum', 'assHeader', 'danmaku'])
    @Js
    def PyJs_anonymous_25_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers([])
        pass
    PyJs_anonymous_25_._set_name('anonymous')
    var.put('assHeader', var.get('fillStr')(var.get('funStr')(PyJs_anonymous_25_), var.get('config'), var.get('info'), Js({'alpha':var.get('hexAlpha')(var.get('config').get('opacity'))})))
    @Js
    def PyJs_anonymous_26_(num, len, this, arguments, var=var):
        var = Scope({'num':num, 'len':len, 'this':this, 'arguments':arguments}, var)
        var.registers(['num', 'len'])
        var.put('num', (Js('')+var.get('num')))
        while (var.get('num').get('length')<var.get('len')):
            var.put('num', (Js('0')+var.get('num')))
        return var.get('num')
    PyJs_anonymous_26_._set_name('anonymous')
    var.put('paddingNum', PyJs_anonymous_26_)
    @Js
    def PyJs_anonymous_27_(time, this, arguments, var=var):
        var = Scope({'time':time, 'this':this, 'arguments':arguments}, var)
        var.registers(['l', 'time'])
        var.put('time', ((Js(100.0)*var.get('time'))^Js(0.0)))
        @Js
        def PyJs_anonymous_28_(c, this, arguments, var=var):
            var = Scope({'c':c, 'this':this, 'arguments':arguments}, var)
            var.registers(['c', 'r'])
            var.put('r', (var.get('time')%var.get('c').get('0')))
            var.put('time', ((var.get('time')-var.get('r'))/var.get('c').get('0')))
            return var.get('paddingNum')(var.get('r'), var.get('c').get('1'))
        PyJs_anonymous_28_._set_name('anonymous')
        var.put('l', Js([Js([Js(100.0), Js(2.0)]), Js([Js(60.0), Js(2.0)]), Js([Js(60.0), Js(2.0)]), Js([var.get('Infinity'), Js(0.0)])]).callprop('map', PyJs_anonymous_28_).callprop('reverse'))
        return ((var.get('l').callprop('slice', Js(0.0), (-Js(1.0))).callprop('join', Js(':'))+Js('.'))+var.get('l').get('3'))
    PyJs_anonymous_27_._set_name('anonymous')
    var.put('formatTime', PyJs_anonymous_27_)
    @Js
    def PyJs_anonymous_29_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers(['r2l', 'fix', 'common', 'withCommon'])
        @Js
        def PyJs_anonymous_30_(line, this, arguments, var=var):
            var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
            var.registers(['rgb', 'dark', 'line', 's'])
            var.put('s', Js(''))
            @Js
            def PyJs_anonymous_31_(x, this, arguments, var=var):
                var = Scope({'x':x, 'this':this, 'arguments':arguments}, var)
                var.registers(['x'])
                return var.get('parseInt')(var.get('x'), Js(16.0))
            PyJs_anonymous_31_._set_name('anonymous')
            @Js
            def PyJs_anonymous_32_(x, this, arguments, var=var):
                var = Scope({'x':x, 'this':this, 'arguments':arguments}, var)
                var.registers(['x'])
                return var.get('x')
            PyJs_anonymous_32_._set_name('anonymous')
            var.put('rgb', var.get('line').get('color').callprop('split', JsRegExp('/(..)/')).callprop('filter', PyJs_anonymous_32_).callprop('map', PyJs_anonymous_31_))
            if PyJsStrictNeq(var.get('line').get('color'),Js('FFFFFF')):
                var.put('s', (Js('\\c&H')+var.get('line').get('color').callprop('split', JsRegExp('/(..)/')).callprop('reverse').callprop('join', Js(''))), '+')
            var.put('dark', ((((var.get('rgb').get('0')*Js(0.299))+(var.get('rgb').get('1')*Js(0.587)))+(var.get('rgb').get('2')*Js(0.114)))<Js(48)))
            if var.get('dark'):
                var.put('s', Js('\\3c&HFFFFFF'), '+')
            if PyJsStrictNeq(var.get('line').get('size'),Js(25.0)):
                var.put('s', (Js('\\fs')+var.get('line').get('size')), '+')
            return var.get('s')
        PyJs_anonymous_30_._set_name('anonymous')
        var.put('common', PyJs_anonymous_30_)
        @Js
        def PyJs_anonymous_33_(line, this, arguments, var=var):
            var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
            var.registers(['line'])
            return ((Js('\\move(')+Js([var.get('line').get('poss').get('x'), var.get('line').get('poss').get('y'), var.get('line').get('posd').get('x'), var.get('line').get('posd').get('y')]).callprop('join', Js(',')))+Js(')'))
        PyJs_anonymous_33_._set_name('anonymous')
        var.put('r2l', PyJs_anonymous_33_)
        @Js
        def PyJs_anonymous_34_(line, this, arguments, var=var):
            var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
            var.registers(['line'])
            return ((Js('\\pos(')+Js([var.get('line').get('poss').get('x'), var.get('line').get('poss').get('y')]).callprop('join', Js(',')))+Js(')'))
        PyJs_anonymous_34_._set_name('anonymous')
        var.put('fix', PyJs_anonymous_34_)
        @Js
        def PyJs_anonymous_35_(f, this, arguments, var=var):
            var = Scope({'f':f, 'this':this, 'arguments':arguments}, var)
            var.registers(['f'])
            @Js
            def PyJs_anonymous_36_(line, this, arguments, var=var):
                var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
                var.registers(['line'])
                return (var.get('f')(var.get('line'))+var.get('common')(var.get('line')))
            PyJs_anonymous_36_._set_name('anonymous')
            return PyJs_anonymous_36_
        PyJs_anonymous_35_._set_name('anonymous')
        var.put('withCommon', PyJs_anonymous_35_)
        return Js({'R2L':var.get('withCommon')(var.get('r2l')),'Fix':var.get('withCommon')(var.get('fix'))})
    PyJs_anonymous_29_._set_name('anonymous')
    var.put('format', PyJs_anonymous_29_())
    @Js
    def PyJs_anonymous_37_(s, this, arguments, var=var):
        var = Scope({'s':s, 'this':this, 'arguments':arguments}, var)
        var.registers(['s'])
        return var.get('s').callprop('replace', JsRegExp('/{/g'), Js('｛')).callprop('replace', JsRegExp('/}/g'), Js('｝')).callprop('replace', JsRegExp('/\\r|\\n/g'), Js(''))
    PyJs_anonymous_37_._set_name('anonymous')
    var.put('escapeAssText', PyJs_anonymous_37_)
    @Js
    def PyJs_anonymous_38_(line, this, arguments, var=var):
        var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
        var.registers(['line'])
        return (((((Js('Dialogue: ')+Js([Js(0.0), var.get('formatTime')(var.get('line').get('stime')), var.get('formatTime')(var.get('line').get('dtime')), var.get('line').get('type'), Js(',20,20,2,,')]).callprop('join', Js(',')))+Js('{'))+var.get('format').callprop(var.get('line').get('type'), var.get('line')))+Js('}'))+var.get('escapeAssText')(var.get('line').get('text')))
    PyJs_anonymous_38_._set_name('anonymous')
    var.put('convert2Ass', PyJs_anonymous_38_)
    @Js
    def PyJs_anonymous_39_(x, this, arguments, var=var):
        var = Scope({'x':x, 'this':this, 'arguments':arguments}, var)
        var.registers(['x'])
        return var.get('x')
    PyJs_anonymous_39_._set_name('anonymous')
    return (var.get('assHeader')+var.get('danmaku').callprop('map', var.get('convert2Ass')).callprop('filter', PyJs_anonymous_39_).callprop('join', Js('\n')))
PyJs_anonymous_24_._set_name('anonymous')
var.put('generateASS', PyJs_anonymous_24_)
@Js
def PyJs_anonymous_40_(wc, hc, b, u, maxr, this, arguments, var=var):
    var = Scope({'wc':wc, 'hc':hc, 'b':b, 'u':u, 'maxr':maxr, 'this':this, 'arguments':arguments}, var)
    var.registers(['maxr', 'wc', 'hc', 'b', 'u'])
    @Js
    def PyJs_anonymous_41_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers(['available', 'score', 'syn', 'used', 'use'])
        var.put('used', Js([Js({'p':(-var.get('Infinity')),'m':Js(0.0),'tf':var.get('Infinity'),'td':var.get('Infinity'),'b':Js(False)}), Js({'p':var.get('hc'),'m':var.get('Infinity'),'tf':var.get('Infinity'),'td':var.get('Infinity'),'b':Js(False)}), Js({'p':(var.get('hc')-var.get('b')),'m':var.get('hc'),'tf':var.get('Infinity'),'td':var.get('Infinity'),'b':Js(True)})]))
        @Js
        def PyJs_anonymous_42_(hv, t0s, t0l, b, this, arguments, var=var):
            var = Scope({'hv':hv, 't0s':t0s, 't0l':t0l, 'b':b, 'this':this, 'arguments':arguments}, var)
            var.registers(['mr', 'hv', 't0l', 'suggestion', 'b', 't0s'])
            var.put('suggestion', Js([]))
            @Js
            def PyJs_anonymous_43_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['m', 'tal', 'i', 'p', 'tas'])
                if (var.get('i').get('m')>var.get('hc')):
                    return var.get('undefined')
                var.put('p', var.get('i').get('m'))
                var.put('m', (var.get('p')+var.get('hv')))
                var.put('tas', var.get('t0s'))
                var.put('tal', var.get('t0l'))
                @Js
                def PyJs_anonymous_44_(j, this, arguments, var=var):
                    var = Scope({'j':j, 'this':this, 'arguments':arguments}, var)
                    var.registers(['j'])
                    if (var.get('j').get('p')>=var.get('m')):
                        return var.get('undefined')
                    if (var.get('j').get('m')<=var.get('p')):
                        return var.get('undefined')
                    if (var.get('j').get('b') and var.get('b')):
                        return var.get('undefined')
                    var.put('tas', var.get('Math').callprop('max', var.get('tas'), var.get('j').get('tf')))
                    var.put('tal', var.get('Math').callprop('max', var.get('tal'), var.get('j').get('td')))
                PyJs_anonymous_44_._set_name('anonymous')
                var.get('used').callprop('forEach', PyJs_anonymous_44_)
                var.get('suggestion').callprop('push', Js({'p':var.get('p'),'r':var.get('Math').callprop('max', (var.get('tas')-var.get('t0s')), (var.get('tal')-var.get('t0l')))}))
            PyJs_anonymous_43_._set_name('anonymous')
            var.get('used').callprop('forEach', PyJs_anonymous_43_)
            @Js
            def PyJs_anonymous_45_(x, y, this, arguments, var=var):
                var = Scope({'x':x, 'y':y, 'this':this, 'arguments':arguments}, var)
                var.registers(['y', 'x'])
                return (var.get('x').get('p')-var.get('y').get('p'))
            PyJs_anonymous_45_._set_name('anonymous')
            var.get('suggestion').callprop('sort', PyJs_anonymous_45_)
            var.put('mr', var.get('maxr'))
            @Js
            def PyJs_anonymous_46_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                if (var.get('i').get('r')>=var.get('mr')):
                    return Js(False)
                var.put('mr', var.get('i').get('r'))
                return Js(True)
            PyJs_anonymous_46_._set_name('anonymous')
            var.put('suggestion', var.get('suggestion').callprop('filter', PyJs_anonymous_46_))
            return var.get('suggestion')
        PyJs_anonymous_42_._set_name('anonymous')
        var.put('available', PyJs_anonymous_42_)
        @Js
        def PyJs_anonymous_47_(p, m, tf, td, this, arguments, var=var):
            var = Scope({'p':p, 'm':m, 'tf':tf, 'td':td, 'this':this, 'arguments':arguments}, var)
            var.registers(['td', 'm', 'tf', 'p'])
            var.get('used').callprop('push', Js({'p':var.get('p'),'m':var.get('m'),'tf':var.get('tf'),'td':var.get('td'),'b':Js(False)}))
        PyJs_anonymous_47_._set_name('anonymous')
        var.put('use', PyJs_anonymous_47_)
        @Js
        def PyJs_anonymous_48_(t0s, t0l, this, arguments, var=var):
            var = Scope({'t0s':t0s, 't0l':t0l, 'this':this, 'arguments':arguments}, var)
            var.registers(['t0l', 't0s'])
            @Js
            def PyJs_anonymous_49_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                return ((var.get('i').get('tf')>var.get('t0s')) or (var.get('i').get('td')>var.get('t0l')))
            PyJs_anonymous_49_._set_name('anonymous')
            var.put('used', var.get('used').callprop('filter', PyJs_anonymous_49_))
        PyJs_anonymous_48_._set_name('anonymous')
        var.put('syn', PyJs_anonymous_48_)
        @Js
        def PyJs_anonymous_50_(i, this, arguments, var=var):
            var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
            var.registers(['i'])
            if (var.get('i').get('r')>var.get('maxr')):
                return (-var.get('Infinity'))
            return (Js(1.0)-(var.get('hypot')((var.get('i').get('r')/var.get('maxr')), (var.get('i').get('p')/var.get('hc')))*var.get('Math').get('SQRT1_2')))
        PyJs_anonymous_50_._set_name('anonymous')
        var.put('score', PyJs_anonymous_50_)
        @Js
        def PyJs_anonymous_51_(t0s, wv, hv, b, this, arguments, var=var):
            var = Scope({'t0s':t0s, 'wv':wv, 'hv':hv, 'b':b, 'this':this, 'arguments':arguments}, var)
            var.registers(['al', 't0s', 'best', 'hv', 'wv', 'td', 'b', 'ts', 'tf', 'scored', 't0l'])
            var.put('t0l', (((var.get('wc')/(var.get('wv')+var.get('wc')))*var.get('u'))+var.get('t0s')))
            var.get('syn')(var.get('t0s'), var.get('t0l'))
            var.put('al', var.get('available')(var.get('hv'), var.get('t0s'), var.get('t0l'), var.get('b')))
            if var.get('al').get('length').neg():
                return var.get(u"null")
            @Js
            def PyJs_anonymous_52_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                return Js([var.get('score')(var.get('i')), var.get('i')])
            PyJs_anonymous_52_._set_name('anonymous')
            var.put('scored', var.get('al').callprop('map', PyJs_anonymous_52_))
            @Js
            def PyJs_anonymous_53_(x, y, this, arguments, var=var):
                var = Scope({'x':x, 'y':y, 'this':this, 'arguments':arguments}, var)
                var.registers(['y', 'x'])
                return (var.get('x') if (var.get('x').get('0')>var.get('y').get('0')) else var.get('y'))
            PyJs_anonymous_53_._set_name('anonymous')
            var.put('best', var.get('scored').callprop('reduce', PyJs_anonymous_53_).get('1'))
            var.put('ts', (var.get('t0s')+var.get('best').get('r')))
            var.put('tf', (((var.get('wv')/(var.get('wv')+var.get('wc')))*var.get('u'))+var.get('ts')))
            var.put('td', (var.get('u')+var.get('ts')))
            var.get('use')(var.get('best').get('p'), (var.get('best').get('p')+var.get('hv')), var.get('tf'), var.get('td'))
            return Js({'top':var.get('best').get('p'),'time':var.get('ts')})
        PyJs_anonymous_51_._set_name('anonymous')
        return PyJs_anonymous_51_
    PyJs_anonymous_41_._set_name('anonymous')
    return PyJs_anonymous_41_
PyJs_anonymous_40_._set_name('anonymous')
var.put('normalDanmaku', PyJs_anonymous_40_(var.get('config').get('playResX'), var.get('config').get('playResY'), var.get('config').get('bottom'), var.get('config').get('r2ltime'), var.get('config').get('max_delay')))
@Js
def PyJs_anonymous_54_(hc, b, u, maxr, this, arguments, var=var):
    var = Scope({'hc':hc, 'b':b, 'u':u, 'maxr':maxr, 'this':this, 'arguments':arguments}, var)
    var.registers(['maxr', 'b', 'hc', 'u'])
    @Js
    def PyJs_anonymous_55_(this, arguments, var=var):
        var = Scope({'this':this, 'arguments':arguments}, var)
        var.registers(['top', 'bottom', 'score', 'fr', 'syn', 'used', 'use'])
        var.put('used', Js([Js({'p':(-var.get('Infinity')),'m':Js(0.0),'td':var.get('Infinity'),'b':Js(False)}), Js({'p':var.get('hc'),'m':var.get('Infinity'),'td':var.get('Infinity'),'b':Js(False)}), Js({'p':(var.get('hc')-var.get('b')),'m':var.get('hc'),'td':var.get('Infinity'),'b':Js(True)})]))
        @Js
        def PyJs_anonymous_56_(p, m, t0s, b, this, arguments, var=var):
            var = Scope({'p':p, 'm':m, 't0s':t0s, 'b':b, 'this':this, 'arguments':arguments}, var)
            var.registers(['m', 't0s', 'b', 'p', 'tas'])
            var.put('tas', var.get('t0s'))
            @Js
            def PyJs_anonymous_57_(j, this, arguments, var=var):
                var = Scope({'j':j, 'this':this, 'arguments':arguments}, var)
                var.registers(['j'])
                if (var.get('j').get('p')>=var.get('m')):
                    return var.get('undefined')
                if (var.get('j').get('m')<=var.get('p')):
                    return var.get('undefined')
                if (var.get('j').get('b') and var.get('b')):
                    return var.get('undefined')
                var.put('tas', var.get('Math').callprop('max', var.get('tas'), var.get('j').get('td')))
            PyJs_anonymous_57_._set_name('anonymous')
            var.get('used').callprop('forEach', PyJs_anonymous_57_)
            return Js({'r':(var.get('tas')-var.get('t0s')),'p':var.get('p'),'m':var.get('m')})
        PyJs_anonymous_56_._set_name('anonymous')
        var.put('fr', PyJs_anonymous_56_)
        @Js
        def PyJs_anonymous_58_(hv, t0s, b, this, arguments, var=var):
            var = Scope({'hv':hv, 't0s':t0s, 'b':b, 'this':this, 'arguments':arguments}, var)
            var.registers(['t0s', 'suggestion', 'hv', 'b'])
            var.put('suggestion', Js([]))
            @Js
            def PyJs_anonymous_59_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                if (var.get('i').get('m')>var.get('hc')):
                    return var.get('undefined')
                var.get('suggestion').callprop('push', var.get('fr')(var.get('i').get('m'), (var.get('i').get('m')+var.get('hv')), var.get('t0s'), var.get('b')))
            PyJs_anonymous_59_._set_name('anonymous')
            var.get('used').callprop('forEach', PyJs_anonymous_59_)
            return var.get('suggestion')
        PyJs_anonymous_58_._set_name('anonymous')
        var.put('top', PyJs_anonymous_58_)
        @Js
        def PyJs_anonymous_60_(hv, t0s, b, this, arguments, var=var):
            var = Scope({'hv':hv, 't0s':t0s, 'b':b, 'this':this, 'arguments':arguments}, var)
            var.registers(['t0s', 'suggestion', 'hv', 'b'])
            var.put('suggestion', Js([]))
            @Js
            def PyJs_anonymous_61_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                if (var.get('i').get('p')<Js(0.0)):
                    return var.get('undefined')
                var.get('suggestion').callprop('push', var.get('fr')((var.get('i').get('p')-var.get('hv')), var.get('i').get('p'), var.get('t0s'), var.get('b')))
            PyJs_anonymous_61_._set_name('anonymous')
            var.get('used').callprop('forEach', PyJs_anonymous_61_)
            return var.get('suggestion')
        PyJs_anonymous_60_._set_name('anonymous')
        var.put('bottom', PyJs_anonymous_60_)
        @Js
        def PyJs_anonymous_62_(p, m, td, this, arguments, var=var):
            var = Scope({'p':p, 'm':m, 'td':td, 'this':this, 'arguments':arguments}, var)
            var.registers(['td', 'm', 'p'])
            var.get('used').callprop('push', Js({'p':var.get('p'),'m':var.get('m'),'td':var.get('td'),'b':Js(False)}))
        PyJs_anonymous_62_._set_name('anonymous')
        var.put('use', PyJs_anonymous_62_)
        @Js
        def PyJs_anonymous_63_(t0s, this, arguments, var=var):
            var = Scope({'t0s':t0s, 'this':this, 'arguments':arguments}, var)
            var.registers(['t0s'])
            @Js
            def PyJs_anonymous_64_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                return (var.get('i').get('td')>var.get('t0s'))
            PyJs_anonymous_64_._set_name('anonymous')
            var.put('used', var.get('used').callprop('filter', PyJs_anonymous_64_))
        PyJs_anonymous_63_._set_name('anonymous')
        var.put('syn', PyJs_anonymous_63_)
        @Js
        def PyJs_anonymous_65_(i, is_top, this, arguments, var=var):
            var = Scope({'i':i, 'is_top':is_top, 'this':this, 'arguments':arguments}, var)
            var.registers(['is_top', 'i', 'f'])
            if (var.get('i').get('r')>var.get('maxr')):
                return (-var.get('Infinity'))
            @Js
            def PyJs_anonymous_66_(p, this, arguments, var=var):
                var = Scope({'p':p, 'this':this, 'arguments':arguments}, var)
                var.registers(['p'])
                return (var.get('p') if var.get('is_top') else (var.get('hc')-var.get('p')))
            PyJs_anonymous_66_._set_name('anonymous')
            var.put('f', PyJs_anonymous_66_)
            return (Js(1.0)-(((var.get('i').get('r')/var.get('maxr'))*(Js(31.0)/Js(32.0)))+((var.get('f')(var.get('i').get('p'))/var.get('hc'))*(Js(1.0)/Js(32.0)))))
        PyJs_anonymous_65_._set_name('anonymous')
        var.put('score', PyJs_anonymous_65_)
        @Js
        def PyJs_anonymous_67_(t0s, hv, is_top, b, this, arguments, var=var):
            var = Scope({'t0s':t0s, 'hv':hv, 'is_top':is_top, 'b':b, 'this':this, 'arguments':arguments}, var)
            var.registers(['is_top', 'al', 'hv', 'best', 'b', 'scored', 't0s'])
            var.get('syn')(var.get('t0s'))
            var.put('al', (var.get('top') if var.get('is_top') else var.get('bottom'))(var.get('hv'), var.get('t0s'), var.get('b')))
            if var.get('al').get('length').neg():
                return var.get(u"null")
            @Js
            def PyJs_anonymous_68_(i, this, arguments, var=var):
                var = Scope({'i':i, 'this':this, 'arguments':arguments}, var)
                var.registers(['i'])
                return Js([var.get('score')(var.get('i'), var.get('is_top')), var.get('i')])
            PyJs_anonymous_68_._set_name('anonymous')
            var.put('scored', var.get('al').callprop('map', PyJs_anonymous_68_))
            @Js
            def PyJs_anonymous_69_(x, y, this, arguments, var=var):
                var = Scope({'x':x, 'y':y, 'this':this, 'arguments':arguments}, var)
                var.registers(['y', 'x'])
                return (var.get('x') if (var.get('x').get('0')>var.get('y').get('0')) else var.get('y'))
            PyJs_anonymous_69_._set_name('anonymous')
            var.put('best', var.get('scored').callprop('reduce', PyJs_anonymous_69_).get('1'))
            var.get('use')(var.get('best').get('p'), var.get('best').get('m'), ((var.get('best').get('r')+var.get('t0s'))+var.get('u')))
            return Js({'top':var.get('best').get('p'),'time':(var.get('best').get('r')+var.get('t0s'))})
        PyJs_anonymous_67_._set_name('anonymous')
        return PyJs_anonymous_67_
    PyJs_anonymous_55_._set_name('anonymous')
    return PyJs_anonymous_55_
PyJs_anonymous_54_._set_name('anonymous')
var.put('sideDanmaku', PyJs_anonymous_54_(var.get('config').get('playResY'), var.get('config').get('bottom'), var.get('config').get('fixtime'), var.get('config').get('max_delay')))
@Js
def PyJs_anonymous_70_(danmaku, this, arguments, var=var):
    var = Scope({'danmaku':danmaku, 'this':this, 'arguments':arguments}, var)
    var.registers(['danmaku', 'side', 'normal'])
    var.put('normal', var.get('normalDanmaku')())
    var.put('side', var.get('sideDanmaku')())
    @Js
    def PyJs_anonymous_71_(x, y, this, arguments, var=var):
        var = Scope({'x':x, 'y':y, 'this':this, 'arguments':arguments}, var)
        var.registers(['y', 'x'])
        return (var.get('x').get('stime')-var.get('y').get('stime'))
    PyJs_anonymous_71_._set_name('anonymous')
    @Js
    def PyJs_anonymous_72_(l, this, arguments, var=var):
        var = Scope({'l':l, 'this':this, 'arguments':arguments}, var)
        var.registers(['l'])
        return var.get('l')
    PyJs_anonymous_72_._set_name('anonymous')
    @Js
    def PyJs_anonymous_73_(line, this, arguments, var=var):
        var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
        var.registers(['line', 'font_size', 'width'])
        var.put('font_size', var.get('Math').callprop('round', (var.get('line').get('size')*var.get('config').get('font_size'))))
        var.put('width', var.get('calcWidth')(var.get('line').get('text'), var.get('font_size')))
        while 1:
            SWITCHED = False
            CONDITION = (var.get('line').get('mode'))
            if SWITCHED or PyJsStrictEq(CONDITION, Js('R2L')):
                SWITCHED = True
                @Js
                def PyJs_anonymous_74_(this, arguments, var=var):
                    var = Scope({'this':this, 'arguments':arguments}, var)
                    var.registers(['pos'])
                    var.put('pos', var.get('normal')(var.get('line').get('time'), var.get('width'), var.get('font_size'), var.get('line').get('bottom')))
                    if var.get('pos').neg():
                        return var.get(u"null")
                    var.get('line').put('type', Js('R2L'))
                    var.get('line').put('stime', var.get('pos').get('time'))
                    var.get('line').put('poss', Js({'x':(var.get('config').get('playResX')+(var.get('width')/Js(2.0))),'y':(var.get('pos').get('top')+var.get('font_size'))}))
                    var.get('line').put('posd', Js({'x':((-var.get('width'))/Js(2.0)),'y':(var.get('pos').get('top')+var.get('font_size'))}))
                    var.get('line').put('dtime', (var.get('config').get('r2ltime')+var.get('line').get('stime')))
                    return var.get('line')
                PyJs_anonymous_74_._set_name('anonymous')
                return PyJs_anonymous_74_()
            if SWITCHED or PyJsStrictEq(CONDITION, Js('TOP')):
                SWITCHED = True
                pass
            if SWITCHED or PyJsStrictEq(CONDITION, Js('BOTTOM')):
                SWITCHED = True
                @Js
                def PyJs_anonymous_75_(isTop, this, arguments, var=var):
                    var = Scope({'isTop':isTop, 'this':this, 'arguments':arguments}, var)
                    var.registers(['isTop', 'pos'])
                    var.put('pos', var.get('side')(var.get('line').get('time'), var.get('font_size'), var.get('isTop'), var.get('line').get('bottom')))
                    if var.get('pos').neg():
                        return var.get(u"null")
                    var.get('line').put('type', Js('Fix'))
                    var.get('line').put('stime', var.get('pos').get('time'))
                    var.get('line').put('posd', var.get('line').put('poss', Js({'x':var.get('Math').callprop('round', (var.get('config').get('playResX')/Js(2.0))),'y':(var.get('pos').get('top')+var.get('font_size'))})))
                    var.get('line').put('dtime', (var.get('config').get('fixtime')+var.get('line').get('stime')))
                    return var.get('line')
                PyJs_anonymous_75_._set_name('anonymous')
                return PyJs_anonymous_75_(PyJsStrictEq(var.get('line').get('mode'),Js('TOP')))
            if True:
                SWITCHED = True
                return var.get(u"null")
            SWITCHED = True
            break
        pass
    PyJs_anonymous_73_._set_name('anonymous')
    @Js
    def PyJs_anonymous_76_(x, y, this, arguments, var=var):
        var = Scope({'x':x, 'y':y, 'this':this, 'arguments':arguments}, var)
        var.registers(['y', 'x'])
        return (var.get('x').get('time')-var.get('y').get('time'))
    PyJs_anonymous_76_._set_name('anonymous')
    return var.get('danmaku').callprop('sort', PyJs_anonymous_76_).callprop('map', PyJs_anonymous_73_).callprop('filter', PyJs_anonymous_72_).callprop('sort', PyJs_anonymous_71_)
PyJs_anonymous_70_._set_name('anonymous')
var.put('setPosition', PyJs_anonymous_70_)
@Js
def PyJs_anonymous_77_(cid, callback, this, arguments, var=var):
    var = Scope({'cid':cid, 'callback':callback, 'this':this, 'arguments':arguments}, var)
    var.registers(['cid', 'callback'])
    @Js
    def PyJs_anonymous_78_(resp, this, arguments, var=var):
        var = Scope({'resp':resp, 'this':this, 'arguments':arguments}, var)
        var.registers(['resp', 'content'])
        var.put('content', var.get('resp').get('responseText').callprop('replace', JsRegExp('/(?:[\\0-\\x08\\x0B\\f\\x0E-\\x1F\\uFFFE\\uFFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|(?:[^\\uD800-\\uDBFF]|^)[\\uDC00-\\uDFFF])/g'), Js('')))
        var.get('callback')(var.get('content'))
    PyJs_anonymous_78_._set_name('anonymous')
    var.get('GM_xmlhttpRequest')(Js({'method':Js('GET'),'url':Js('http://comment.bilibili.com/{{cid}}.xml').callprop('replace', Js('{{cid}}'), var.get('cid')),'onload':PyJs_anonymous_78_}))
PyJs_anonymous_77_._set_name('anonymous')
var.put('fetchXML', PyJs_anonymous_77_)
@Js
def PyJs_anonymous_79_(cid, callback, this, arguments, var=var):
    var = Scope({'cid':cid, 'callback':callback, 'this':this, 'arguments':arguments}, var)
    var.registers(['cid', 'callback'])
    @Js
    def PyJs_anonymous_80_(content, this, arguments, var=var):
        var = Scope({'content':content, 'this':this, 'arguments':arguments}, var)
        var.registers(['content'])
        var.get('callback')(var.get('parseXML')(var.get('content')))
    PyJs_anonymous_80_._set_name('anonymous')
    var.get('fetchXML')(var.get('cid'), PyJs_anonymous_80_)
PyJs_anonymous_79_._set_name('anonymous')
var.put('fetchDanmaku', PyJs_anonymous_79_)
@Js
def PyJs_anonymous_81_(content, this, arguments, var=var):
    var = Scope({'content':content, 'this':this, 'arguments':arguments}, var)
    var.registers(['data', 'content'])
    var.put('data', var.get('DOMParser').create().callprop('parseFromString', var.get('content'), Js('text/xml')))
    @Js
    def PyJs_anonymous_82_(line, this, arguments, var=var):
        var = Scope({'line':line, 'this':this, 'arguments':arguments}, var)
        var.registers(['line', 'text', 'info'])
        var.put('info', var.get('line').callprop('getAttribute', Js('p')).callprop('split', Js(',')))
        var.put('text', var.get('line').get('textContent'))
        return Js({'text':var.get('text'),'time':var.get('Number')(var.get('info').get('0')),'mode':Js([var.get('undefined'), Js('R2L'), Js('R2L'), Js('R2L'), Js('BOTTOM'), Js('TOP')]).get(var.get('Number')(var.get('info').get('1'))),'size':var.get('Number')(var.get('info').get('2')),'color':var.get('RRGGBB')((var.get('parseInt')(var.get('info').get('3'), Js(10.0))&Js(16777215))),'bottom':(var.get('Number')(var.get('info').get('5'))>Js(0.0))})
    PyJs_anonymous_82_._set_name('anonymous')
    return var.get('Array').callprop('apply', var.get('Array'), var.get('data').callprop('querySelectorAll', Js('d'))).callprop('map', PyJs_anonymous_82_)
PyJs_anonymous_81_._set_name('anonymous')
var.put('parseXML', PyJs_anonymous_81_)
@Js
def PyJs_anonymous_83_(content, this, arguments, var=var):
    var = Scope({'content':content, 'this':this, 'arguments':arguments}, var)
    var.registers(['content'])
    var.put('content', var.get('content').callprop('replace', JsRegExp('/[\\u0000-\\u0008\\u000b\\u000c\\u000e-\\u001f]/g'), Js('')))
    return var.get('parseXML')(var.get('content'))
PyJs_anonymous_83_._set_name('anonymous')
var.put('parseFile', PyJs_anonymous_83_)
@Js
def PyJs_anonymous_84_(name, content, this, arguments, var=var):
    var = Scope({'name':name, 'content':content, 'this':this, 'arguments':arguments}, var)
    var.registers(['content', 'name', 'danmaku', 'ass'])
    var.put('danmaku', var.get('parseFile')(var.get('content')))
    var.put('ass', var.get('generateASS')(var.get('setPosition')(var.get('danmaku')), Js({'title':var.get('document').get('title'),'ori':var.get('name')})))
    return var.get('ass')
PyJs_anonymous_84_._set_name('anonymous')
var.put('gotFile', PyJs_anonymous_84_)
pass


# Add lib to the module scope
example = var.to_python()