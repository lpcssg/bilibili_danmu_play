# 获取bilibili xml弹幕文件

from setting import BILIBILI_URL, HERDER, FILE_NAME, SAVE_PATH
import requests
from scrapy import Selector
import json
import os


def get_cid_by_bilibili_url(bilibili_url=BILIBILI_URL):
    """
    根据url获取cid
    """
    global FILE_NAME
    req = requests.get(bilibili_url, headers=HERDER)

    # with open("./1.html",'w',encoding='utf8') as f:
    #     f.write(req.text)

    # 获取bvid
    bvid = (
        Selector(response=req)
        .xpath(r'//a[@class="mediainfo_avLink__iyzyV"]/text()')
        .extract_first()
    )
    print("bvid:", bvid)

    # 获取标题
    FILE_NAME = (
        Selector(response=req)
        .xpath(r'//title/text()')
        .extract_first()
    )
    if FILE_NAME is None:
        assert "获取标题错误"

    json_data = (
        Selector(response=req)
        .xpath(r'//script[@id="__NEXT_DATA__"]/text()')
        .extract_first()
    )
    json_data = json.loads(json_data)
    episodes = json_data["props"]["pageProps"]["dehydratedState"]["queries"][0][
        "state"
    ]["data"]["result"]["play_view_business_info"]["episode_info"]["cid"]
    cid=episodes
    # cid = ""
    # for i in episodes:
    #     if i["bvid"] == bvid:
    #         cid = i["cid"]
    print("cid:", cid)
    return str(cid)


def get_danmu_by_cid(cid):
    """
    根据cid获取xml格式弹幕,并返回文件名
    """
    danmu_url = "https://api.bilibili.com/x/v1/dm/list.so?oid=" + cid
    req = requests.get(danmu_url, headers=HERDER)
    if not os.path.exists(SAVE_PATH + FILE_NAME):
        os.makedirs(SAVE_PATH + FILE_NAME)
    with open(
        SAVE_PATH + FILE_NAME + "/" + FILE_NAME + ".xml", "w", encoding=req.encoding
    ) as f1:
        f1.write(req.text)
    return FILE_NAME


def get_bilibili_video_name_by_url(bilibili_url=BILIBILI_URL):
    """
    根据url获取标题
    """
    global FILE_NAME
    if not (FILE_NAME == "" or FILE_NAME is None):
        return FILE_NAME
    req = requests.get(bilibili_url, headers=HERDER)
    # 获取标题
    FILE_NAME = (
        Selector(response=req)
        .xpath(r'//div[@class="Phone_video_title__8fHdx"]/text()')
        .extract_first()
    )
    if FILE_NAME is None:
        assert "获取标题错误"
    return FILE_NAME
